FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

ADD ./cert-api-prod/app/controller/issuer_application.py	/app/main.py
ADD ./cert-api-prod/app/controller												/app/controller
ADD ./cert-issuer																					/app/cert_issuer
ADD ./cert-tools/sample_data/unsigned_certificates				/app/cert_issuer/data/unsigned_certificates

USER 0:0
WORKDIR /app/cert_issuer

# CMD bash -c "pip3 install -r ethereum_smart_contract_requirements.txt && /start.sh"

# env_file:
#   - cert_issuer.env
# #restart: unless-stopped
# ports:
#   - 7001:80
