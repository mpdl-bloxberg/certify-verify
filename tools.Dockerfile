FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

ADD ./cert-tools-prod																			/app/cert_tools
ADD ./cert-api-prod/app/db																/app/db
ADD ./cert-tools-prod/app/controller/tools_application.py	/app/main.py
ADD ./cert-tools-prod/app/controller											/app/controller

# ADD ./cert-api-prod/cert_tools.env  /cert-api-env
# ADD ./cert-issuer/conf.env					/cert-issuer.env

USER 0:0
WORKDIR /app/cert_tools

# CMD bash -c "pip install -r requirements.txt && /start.sh"

# env_file:
#   - cert_tools.env
#   - ../cert-issuer/conf.env
# #restart: unless-stopped
# ports:
#   - 7000:80
#
